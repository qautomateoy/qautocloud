# -*- coding: utf-8 -*-
# Example for using WebDriver object: driver = self.get_current_driver() e.g driver.current_url
from selenium.webdriver.common.by import By
import QAutoRobot
from QAutoLibrary.QAutoElement import QAutoElement
from time import sleep

class Demopage():
    # Pagemodel timestamp: 20180407162304
    # Pagemodel url: https://demo.qautomate.fi/
    # Pagemodel area: Full screen
    # Pagemodel screen resolution: (1920, 1080)

    USERNAME  = QAutoElement((By.ID, u'username'), coordinates=(822, 498), size=(296,47))
    PASSWORD = QAutoElement((By.ID, u'password'), coordinates=(822, 558), size=(296, 47))
    ID_SUBMIT = QAutoElement((By.ID, u'submit'), coordinates=(816, 618), size=(299, 41))

    def input_username_login(self, text=None):
        QAutoRobot.input_text(self.USERNAME, text)

    def input_password_login(self, text=None):
        QAutoRobot.input_text(self.PASSWORD, text)

    def sign_in(self):
        QAutoRobot.click_element(self.ID_SUBMIT)
